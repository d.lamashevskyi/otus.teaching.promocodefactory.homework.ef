﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Operations.Responses
{
    public class BaseResponse
    {
        public Guid Id { get; set; }
    }
}
