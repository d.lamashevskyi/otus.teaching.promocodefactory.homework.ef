﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomersRepository : GenericRepository<Customer>, ICustomersRepository
    {
        public CustomersRepository(EfRepository context) : base(context)
        {

        } 

        public Customer Get(Guid id)
        {
            return _context.Customers.Include(x => x.PromoCodes).Include(x => x.CustomerPreference)
                    .ThenInclude(y => y.Preference).FirstOrDefault(x => x.Id == id);
        }

        public void Remove(Guid id)
        {
            var entityCustomer = _context.Customers
                .Include(c=>c.PromoCodes)
                .Include(c => c.CustomerPreference).ThenInclude(c=>c.Preference)
                .ToList().FirstOrDefault(c=>c.Id==id);
        }

        public void Update(Guid id, Customer customer)
        {
            customer.Id = id;
            var entityCustomer = _context.Customers.Update(customer);
        }

        public void UpdatePromocode(Guid customerId, PromoCode promoCode)
        {
            var entityCustomer = _context.Customers
                .Include(c => c.PromoCodes);
        }

        public void RemovePromocode(Guid customerId, PromoCode promoCode)
        {
            throw new NotImplementedException();
        }
        public void AddPromocode(Guid customerId, PromoCode promoCode)
        {
            throw new NotImplementedException();
        }

        public void AddPreference(Guid customerId, Preference preference)
        {
            var entityCustomer = _context.CustomerPreferences.Where(c => c.CustomerId == customerId);
            if (entityCustomer.FirstOrDefault(c => c.PreferenceId == preference.Id) == null)
            {
                _context.CustomerPreferences.Add(new CustomerPreference()
                {
                    CustomerId = customerId,
                    PreferenceId = preference.Id
                });
            }
        }

        public void RemovePreference(Guid customerId, Preference preference)
        {
            var entityCustomer = _context.CustomerPreferences.FirstOrDefault(c => c.CustomerId == customerId && c.PreferenceId == preference.Id);
            if (entityCustomer != null)
            {
                _context.CustomerPreferences.Remove(entityCustomer);
            }
        }

        public void UpdatePreference(Guid customerId, Preference preference)
        {
            var entityCustomer = _context.CustomerPreferences.FirstOrDefault(c => c.CustomerId == customerId && c.PreferenceId == preference.Id); 
            if(entityCustomer != null){
                entityCustomer.Preference = preference;
                entityCustomer.PreferenceId = preference.Id; 
                _context.CustomerPreferences.Update(entityCustomer);
            } 
        }

    }
}
