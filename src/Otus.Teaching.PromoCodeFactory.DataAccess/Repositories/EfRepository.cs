﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository : DbContext 
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Posts { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerPreference> CustomerPreferences { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; } 

        public EfRepository(DbContextOptions<EfRepository> options) : base(options)
        {  
        }
         

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>(entity =>
            {
                entity.HasKey(b => b.Id);
                entity.Property(b => b.Id).ValueGeneratedOnAdd();
                entity.HasData(FakeDataFactory.Roles);
            });

            modelBuilder.Entity<Employee>(entity =>
            {  
                entity.HasKey(b => b.Id);
                entity.Property(b => b.Id).ValueGeneratedOnAdd();
                entity.HasOne(d => d.Role); 
                entity.HasData(FakeDataFactory.Employees);
            }); 

             

            modelBuilder.Entity<Preference>(entity =>
            {
                entity.HasKey(b => b.Id);
                entity.Property(b => b.Id).ValueGeneratedOnAdd();
                entity.HasMany(b => b.CustomerPreference)
                    .WithOne(b=>b.Preference);
                entity.HasData(FakeDataFactory.Preferences);
            });

            modelBuilder.Entity<CustomerPreference>(entity =>
            {
                entity.HasKey(b => b.Id);
                entity.Property(b => b.Id).ValueGeneratedOnAdd();
                entity.HasOne(b => b.Customer).WithMany(b => b.CustomerPreference)
                        .HasForeignKey(b => b.CustomerId);
                entity.HasOne(b => b.Preference).WithMany(b => b.CustomerPreference)
                        .HasForeignKey(b => b.PreferenceId);
                entity.HasData(FakeDataFactory.CustomerPreferences);
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.HasKey(b => b.Id);
                entity.HasKey(b => b.Id);
                entity.HasMany(b => b.PromoCodes);
                entity.HasMany(b => b.CustomerPreference)
                    .WithOne(b => b.Customer);
                entity.Property(b => b.Id).ValueGeneratedOnAdd();
                entity.HasData(FakeDataFactory.Customers);
            });
            modelBuilder.Entity<PromoCode>(entity =>
            { 
                entity.HasKey(b => b.Id);
                entity.Property(b => b.Id).ValueGeneratedOnAdd();
                entity.HasOne(d => d.Preference);
                entity.HasOne(d => d.PartnerManager); 
            });
        }

    }
}
