﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public interface ICustomersRepository
    {
        IEnumerable<Customer> GetAll();
        Customer Get(Guid id); 
        void Remove(Guid id);
        void Add(Customer customer);
        void Update(Guid id,Customer customer); 
        void AddPromocode(Guid customerId, PromoCode promoCode);
        void UpdatePromocode(Guid customerId, PromoCode promoCode);
        void RemovePromocode(Guid customerId, PromoCode promoCode);
        void AddPreference(Guid customerId, Preference preference);
        void RemovePreference(Guid customerId, Preference preference);
        void UpdatePreference(Guid customerId, Preference preference);
    }
}