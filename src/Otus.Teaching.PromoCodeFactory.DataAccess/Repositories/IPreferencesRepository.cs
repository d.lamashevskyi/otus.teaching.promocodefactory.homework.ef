﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public interface IPreferencesRepository
    {
        public Preference GetById(Guid preferenceId);
    }
}