﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class PreferencesRepository : GenericRepository<Preference>, IPreferencesRepository
    {
        public PreferencesRepository(EfRepository context) : base(context)
        {
        }

        public Preference GetListById(Guid preferenceId)
        {
           return GetById(preferenceId);
        }
    }
}
