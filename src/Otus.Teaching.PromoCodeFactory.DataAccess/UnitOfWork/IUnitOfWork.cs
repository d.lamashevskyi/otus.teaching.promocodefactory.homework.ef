﻿using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.UnitOfWork
{
    public interface IUnitOfWork
    {
        IEmployeesRepository Employees { get; }
        IRoleRepository Roles { get; }
        ICustomersRepository Customers { get; }
        IPromoCodeRepository PromoCodes { get; }
        IPreferencesRepository Preferences { get; }
        int Complete();
    }
}
