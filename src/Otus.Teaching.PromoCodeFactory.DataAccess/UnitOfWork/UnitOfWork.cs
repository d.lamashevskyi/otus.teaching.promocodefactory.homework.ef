﻿using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly EfRepository _context;
        public UnitOfWork(EfRepository context)
        {
            _context = context;
            Employees = new EmployeesRepository(_context);
            Roles = new RoleRepository(_context);
            Customers = new CustomersRepository(_context);
            PromoCodes = new PromoCodeRepository(_context);
            Preferences = new PreferencesRepository(_context); 
        } 

        public IEmployeesRepository Employees { get; private set; }

        public IRoleRepository Roles { get; private set; }

        public ICustomersRepository Customers { get; private set; }

        public IPromoCodeRepository PromoCodes { get; private set; }

        public IPreferencesRepository Preferences { get; private set; }

        public int Complete()
        {
            return _context.SaveChanges();
        }
        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
