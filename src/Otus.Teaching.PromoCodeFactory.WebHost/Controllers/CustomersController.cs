﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.UnitOfWork;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {

        private readonly IUnitOfWork _unitOfWork;

        public CustomersController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Get list of Customers
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var data = _unitOfWork.Customers.GetAll().ToList();
            var result = data.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Email = x.Email,
            }).ToList();
            return await Task.FromResult(result);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            //TODO: Добавить получение клиента вместе с выданными ему промомкодами 
            throw new NotImplementedException();
        }

        [HttpPost]
        public Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            //TODO: Добавить создание нового клиента вместе с его предпочтениями
            throw new NotImplementedException();
        }

        [HttpPut("{id}")]
        public Task<int> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            //TODO: Обновить данные клиента вместе с его предпочтениям
            _unitOfWork.Customers.Update(id, new Customer()
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email, 
            });
            foreach (var preferenceId in request.PreferenceIds)
            {
                var preference = _unitOfWork.Preferences.GetById(preferenceId);
                _unitOfWork.Customers.UpdatePreference(id, preference);
            }
            return  Task.FromResult(_unitOfWork.Complete());
        }

        //Delete customer data by ID
        [HttpDelete]
        public Task<int> DeleteCustomer(Guid id)
        {
            _unitOfWork.Customers.Remove(id);
            return Task.FromResult(_unitOfWork.Complete());
        }
    }
}