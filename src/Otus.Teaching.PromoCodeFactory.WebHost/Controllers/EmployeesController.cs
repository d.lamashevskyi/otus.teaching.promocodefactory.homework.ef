﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            throw new NotImplementedException();
        }
        
        /// <summary>
        /// Получить данные сотрудника по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Добавление данных о сотруднике
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<List<EmployeeShortResponse>> CreateEmployeesAsync(Employee employee)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Удаление данных сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<List<EmployeeShortResponse>> DeleteEmployeesAsync(Guid id)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Обновить данные сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<ActionResult<EmployeeResponse>> UpdateEmployeesAsync(Guid id, Employee employee)
        {
            throw new NotImplementedException();
        }
    }
}